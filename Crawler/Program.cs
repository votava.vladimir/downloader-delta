﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Threading;

namespace Crawler
{
    class Program
    {
        static void Main(string[] args)
        {
            Downloader down = new Downloader();
            down.Run("https://www.itnetwork.cz/cplusplus/prechod-od-c-k-cplusplus", "E:/visualStudio/", 1).Wait();

            Console.WriteLine("konec");
            Console.ReadKey();
        }
    }
    class PageInfo
    {
        public string _url;
        public int _depth;
        public PageInfo(string url, int depth)
        {
            _url = url;
            _depth = depth;
        }
    }
    class Downloader
    {
        List<string> Odkazy = new List<string>();
        private int _depth;
        private bool addToOdkazy(string url)
        {
            if (!Odkazy.Contains(url))
            {
                Console.WriteLine(url);
                Odkazy.Add(url);
                return true;
            }
            return false;
        }
        public Downloader()
        {

        }
        private string correctUrl(string orig, string url)
        {
            if (url == null || url.StartsWith("..") || url.Contains("javascript"))
            {
                return null;
            }
            if (url.Contains('#'))
            {
                url = url.Split('#')[0];
            }
            if (orig.EndsWith("/"))
            {
                orig = orig.Remove(orig.Length - 1, 1);
            }
            else if (url.StartsWith(@"//"))
            {
                url = "http:" + url;
            }
            else if (url.StartsWith(@"/"))
            {
                url = orig + url;
            }
            return url;


            //url.Replace('<', ' ').Replace('>', ' ').Replace('>', ' ').Replace(':', ' ').Replace('\"', ' ').Replace('/', ' ').Replace('\\', ' ').Replace('|', ' ').Replace('?', ' ').Replace('*', ' ');
        }
        private string getCorrect(string url)
        {
            if (url.Contains('?'))
            {
                return url.Split('?')[0];
            }
            return url;
        }
        private List<string> getUrls(string url)
        {
            List<string> odkazy = new List<string>();
            Aspose.Html.HTMLDocument document;
            try
            {
                document = new Aspose.Html.HTMLDocument(new Aspose.Html.Url(url));
            }
            catch
            {
                document = new Aspose.Html.HTMLDocument();
            }
            foreach (var odkaz in document.GetElementsByTagName("a"))
            {
                string validovany_odkaz = correctUrl(url, odkaz.GetAttribute("href"));
                if (validovany_odkaz != null)
                {
                    if(validovany_odkaz.Length > 5 && validovany_odkaz.Split('.').Length > 2)
                    {
                        odkazy.Add(validovany_odkaz);
                    }
                }
            }
            return odkazy;
        }
        private void addUrls(PageInfo pg)
        {
            bool isEnd = pg._depth >= _depth ? true : false;
            foreach (string odkaz in getUrls(pg._url))
            {
                if (addToOdkazy(odkaz) && !isEnd)
                {
                    addUrls(new PageInfo(getCorrect(odkaz), pg._depth + 1));
                }
            }
        }
        private async Task DownloadPages(string path)
        {
            using (var wc = new WebClient())
            {
                Console.WriteLine(Odkazy.Count());
                foreach (var item in Odkazy)
                {
                    var fileName = createFileName(item) + ".html";
                    var filePath = Path.Combine(path, fileName);
                    string page =  await wc.DownloadStringTaskAsync(new Uri(item));
                    File.WriteAllText(filePath, page);
                }
            }

        }
        private string createFileName(string name)
        {
            name = name.Split('.')[1];

            return name;
        }
        public async Task Run(string url,string path, int depth)
        {
            _depth = depth;
            addUrls(new PageInfo(url, 0));
            await DownloadPages(path);

        }
    }
}
